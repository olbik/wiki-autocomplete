import './styles.css';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { runClient } from './app/runClient';
process.env.NODE_PATH = __dirname;
injectTapEventPlugin();
runClient();
