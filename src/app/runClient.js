import React from 'react'; //eslint-disable-line
import ReactDOM from 'react-dom'; //eslint-disable-line
import { Router, browserHistory } from 'react-router';
import generateRouting from './routing.js';

export const runClient = (id = 'app') => {
  const routes = generateRouting();
  ReactDOM.render(
    <Router
      routes={routes}
      history={browserHistory}
    />,
    document.getElementById(id)
  );
};
