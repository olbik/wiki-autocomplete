import React from 'react'; //eslint-disable-line
import { renderToString } from 'react-dom/server';
import { match, RouterContext } from 'react-router';
import express from 'express';
import template from './template.js';
import apiProxy from './apiProxy';

export const server = express();

server.use('/wiki', apiProxy(req => 'https://pl.wikipedia.org/w/api.php?' + req.originalUrl.replace('/wiki?', '')));

server.use((req, res, next) => {
  const go = (url, status = 200) => {
    const generateRouting = require('./routing.js');
    match({ routes: generateRouting(), location: decodeURIComponent(url) }, (error, redirect, props) => {
      const render = () => {
        const content = renderToString(<RouterContext {...props} />);
        res.status(status).send(template({
          content,
          prod: process.env.NODE_ENV === 'production'
        }));
      }
      if (error) {
        res.status(500).send(error.message);
      } else if (redirect) {
        res.redirect(302, redirect.pathname + redirect.search);
      } else if (props) {
        render(200);
      } else {
        res.redirect('/');
      }
    });
  }
  go(req.url);
});
