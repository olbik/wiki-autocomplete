import axios from 'axios';

const request = axios.create({
  timeout: 5000,
});

const apiProxy = buildUrl => async (req, res) => {
  const requestUrl = buildUrl(req);
  const requestData = {
    method: req.method.toLowerCase(),
    url: requestUrl,
  };
  if (!(requestData.method === 'get' || requestData.method === 'delete')) {
    requestData.data = req.body;
  }
  try {
    const response = await request.request(requestData);
    res.status(response.status).send(response.data);
  } catch (err) {
    if (err.data) {
      console.log(err);
      const status = parseInt(err.data.status, 10) || parseInt(err.data.code, 10) || 502;
      res.status(status).send({ message: err.message, status });
    } else {
      res.status(502).send({ message: err.message, status: 502 });
    }
    console.warn('[API PROXY ERROR]', err.data, err.config);
  }
};

export default apiProxy;
