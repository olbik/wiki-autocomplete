import React from 'react'; //eslint-disable-line
import { Route, IndexRoute } from 'react-router';
import Wrapper from '../components/Wrapper/Wrapper';
import WikiAutocomplete from '../components/WikiAutocomplete/WikiAutocomplete';
import PageNotFound from '../components/PageNotFound/PageNotFound';

export default () => (
  <Route path="/" component={Wrapper}>
    <IndexRoute component={WikiAutocomplete} />
    <Route path="*" component={PageNotFound} />
  </Route>
);
