import React, { Component } from 'react'; //eslint-disable-line
import { cyan900 } from 'material-ui/styles/colors';
import styles from './AutocompleteItem.css';

export default ({ title, desc }) => (
  <div className={styles.wrap}>
    <div className={styles.title} style={{ color: cyan900 }}>{title}</div>
    <div className={styles.desc}>{desc}</div>
  </div>
);
