import React, { Component } from 'react'; //eslint-disable-line
import { Link } from 'react-router';
import debounce from 'lodash/debounce';
import cx from 'classnames';
import AutoComplete from 'material-ui/AutoComplete';
import MenuItem from 'material-ui/MenuItem';
import IconClear from 'material-ui/svg-icons/content/clear';
import { grey700 } from 'material-ui/styles/colors';
import api from '../../api/wiki';
import AutocompleteItem from './AutocompleteItem/AutocompleteItem';
import styles from './WikiAutocomplete.css';

class WikiAutocomplete extends Component {
  state = {
    searchText: '',
    dataSource: [],
  }

  handleUpdateInput = debounce(async searchText => {
    if (searchText === '') {
      this.setState({ searchText, dataSource: [] });
      return;
    }
    const data = await api.getWikiAutocomplete(searchText);
    const dataSource = data.map(item => ({
      data: item,
      text: item.title,
      value: (<MenuItem primaryText={<AutocompleteItem {...item} />} />)
    }));
    this.setState({ searchText, dataSource });
  }, 300)

  handleItemSelected = item => window.open(item.data.url);

  handleDelete = () => this.setState({ searchText: '', dataSource: [] })

  render() {
    const { searchText, dataSource } = this.state;
    return (
      <div className={styles.wrap}>
        <AutoComplete
          id="WikiAutocomplete"
          hintText="Wpisz szukaną frazę"
          floatingLabelText="Wyszukaj w Wikipedii"
          searchText={searchText}
          dataSource={dataSource}
          onUpdateInput={this.handleUpdateInput}
          onNewRequest={this.handleItemSelected}
          filter={AutoComplete.noFilter}
          fullWidth
        />
        <IconClear onClick={this.handleDelete} color={grey700} className={cx(styles.clear, { [styles.hidden]: searchText === ''})} />
      </div>
    );
  }
}

export default WikiAutocomplete;
