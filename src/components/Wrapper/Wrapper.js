import React from 'react'; //eslint-disable-line
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import styles from './Wrapper.css';

const muiTheme = getMuiTheme({
  userAgent: 'all',
});

const Wrapper = ({ children }) => (
  <MuiThemeProvider muiTheme={muiTheme}>
    <div className={styles.wrap}>
      {children}
    </div>
  </MuiThemeProvider>
);

export default Wrapper;
