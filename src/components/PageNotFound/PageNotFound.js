import React from 'react'; //eslint-disable-line
import { Link } from 'react-router';
import styles from './PageNotFound.css';

export default () => (
  <div>
    <h1 className={styles.title}>Strona nie istnieje</h1>
    <Link to="/" className={styles.link}>Przejdź do strony głównej</Link>
  </div>
);
