import axios from 'axios';

const getWikiAutocomplete = phrase => new Promise((resolve, reject) => {
  axios
    .get(`/wiki?action=opensearch&format=json&formatversion=2&namespace=0&limit=10&suggest=true&search=${phrase}`)
    .then(response => {
      const data = [];
      for(let i = 0; i < response.data[1].length; i++) {
        data.push({
          title: response.data[1][i],
          desc: response.data[2][i],
          url: response.data[3][i],
        });
      }
      resolve(data);
    })
    .catch(response => reject(response));
});

const api = {
  getWikiAutocomplete,
};

export default api;
